#!/bin/bash

readarray -t dirs < <(find .. -maxdepth 1 -type d -printf '%P\n')

for i in "${dirs[@]}"; do
  if [ -d "../$i/.devcontainer" ];
  then
  cp .env "../$i/.devcontainer/.env"
  fi
done
