#!/bin/bash

awslocal sns create-topic --name NotifyCalcRequest

awslocal sns create-topic --name NotifyCalculation

awslocal sqs create-queue --queue-name ReqDataToCalcIMC

awslocal sqs create-queue --queue-name ReqDataToCalcTMB

awslocal sqs create-queue --queue-name CalcDataToStorage

awslocal sns subscribe \
  --topic-arn arn:aws:sns:us-east-1:000000000000:NotifyCalcRequest \
  --protocol sqs \
  --notification-endpoint arn:aws:sqs:us-east-1:000000000000:ReqDataToCalcIMC \
  --attributes '{"RawMessageDelivery": "true"}'

awslocal sns subscribe \
  --topic-arn arn:aws:sns:us-east-1:000000000000:NotifyCalcRequest \
  --protocol sqs \
  --notification-endpoint arn:aws:sqs:us-east-1:000000000000:ReqDataToCalcTMB \
  --attributes '{"RawMessageDelivery": "true"}'

awslocal sns subscribe \
  --topic-arn arn:aws:sns:us-east-1:000000000000:NotifyCalculation \
  --protocol sqs \
  --notification-endpoint arn:aws:sqs:us-east-1:000000000000:CalcDataToStorage \
  --attributes '{"RawMessageDelivery": "true"}'
