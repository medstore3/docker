<img src="https://cdn3.iconfinder.com/data/icons/social-media-2169/24/social_media_social_media_logo_docker-512.png" align="right" width="50px">

<br><br>

# Docker - Ambiente Local Com Containers Compartilhados Entre os Microserviços

Este é um projeto que visa facilitar o desenvolvimento e o teste local de microserviços que dependem de serviços compartilhados. Utilizando o Docker Compose, você poderá criar um ambiente local consistente e replicável, onde os serviços compartilhados são executados como contêineres Docker.

## Pré-requisitos

Antes de começar, verifique se você possui os seguintes pré-requisitos instalados em sua máquina de desenvolvimento:

- Docker: [Instalação do Docker](https://docs.docker.com/engine/install/)
- Docker Compose: [Instalação do Docker Compose](https://docs.docker.com/compose/install/)

Certifique-se de que o Docker e o Docker Compose estejam configurados corretamente e sejam acessíveis a partir do seu terminal ou prompt de comando.

## Configuração do Projeto

1. Clone este repositório para sua máquina local:

```bash
git clone <url_do_repositorio>
```

2. Navegue até o diretório do projeto:

```bash
cd <pasta_do_projeto>
```

4. Execute o script configure.sh:

```bash
bash ./configure.sh
```

5. Esse script é responsável por configurar os projetos já baixados do Medstore, para serem executados localmente ao mesmo tempo, configurando portas, linkando pacotes, entre outras configurações.

6. Dentro do diretório, você encontrará o arquivo `common-compose.yml`. Este arquivo define a configuração do ambiente Docker Compose, incluindo os serviços compartilhados necessários para o seu projeto.

## Executando os Serviços Compartilhados

Agora que o projeto está configurado, você pode executar os serviços compartilhados usando o Docker Compose. Certifique-se de estar no diretório raiz do projeto.

1. No terminal ou prompt de comando, execute o seguinte comando:

```bash
docker-compose -f ./common-compose.yml up
```

2. Para escolher os serviços que deseja executar, execute o seguinte comando:

```bash
docker-compose -f ./common-compose.yml up -d <nome_servico_1> <nome_servico_2>... up
```

3. Você também pode executar os serviços compartilhados com os containers alpine de serviços do Medstore. Segue exemplo abaixo:

```bash
docker-compose -f ./common-compose.yml \
    -f ../app.medstore.bff/docker-compose.yml \
    up
```

4. Isso iniciará o ambiente do Docker Compose e os serviços compartilhados serão iniciados como contêineres Docker.

5. Aguarde até que todos os serviços sejam iniciados completamente. Você verá a saída dos contêineres no terminal.

6. Uma vez que todos os serviços estejam em execução, você poderá acessá-los a partir de seus microserviços locais. Consulte a documentação de cada serviço para saber como se conectar a eles.

## Parando os Serviços Compartilhados

Quando você terminar de usar os serviços compartilhados, poderá pará-los usando o Docker Compose.

1. No terminal ou prompt de comando, pressione `Ctrl + C` para interromper a execução do Docker Compose.

2. Em seguida, execute o seguinte comando para parar e remover os contêineres:

```bash
docker-compose down
```

Isso encerrará os serviços compartilhados e removerá os contêineres Docker associados.

## Contribuição

Contribuições são bem-vindas! Se você quiser adicionar algo ou melhorar as existentes, sinta-se à vontade para enviar um pull request para o repositório no GitLab. Certifique-se de seguir as diretrizes de contribuição e de teste fornecidas no projeto.

## Licença

Esta biblioteca está licenciada sob a **Licença MIT**. Sinta-se à vontade para usá-la em seus projetos comerciais ou pessoais.

<img src="https://cdn3.iconfinder.com/data/icons/social-media-2169/24/social_media_social_media_logo_docker-512.png" width="50px">
